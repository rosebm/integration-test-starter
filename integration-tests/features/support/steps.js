// features/support/steps.js

const { When, Then } = require("@cucumber/cucumber")
const { By } = require('selenium-webdriver')
const assert = require("assert").strict

When("I visit the site", async function () {
  await this.driver.get(process.env.TEST_SERVER)
})

Then("Life is good", function () {
  const hello = this.driver.findElement(By.id("hello"))
  assert.ok(!!hello)
})
